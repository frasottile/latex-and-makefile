# Latex and Makefile

Example of a simple Makefile permitting to compile a tex project, that consists of several files, uses images, citations and index. 

These are the files (Makefile and .tex files) used in the tutorial presented in this [video](https://youtu.be/9cVUXD-0kgY) 